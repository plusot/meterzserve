var socket = io();
var replace = $.getUrlVar('replace');
var device = $.getUrlVar('device');
var session = $.getUrlVar('session');
var coordinates = [];
var coordinateTimes = [];
var map = null;
var localData = [];
var latestPosition = null;
var updateLegendTimeout = null;
var plot = null;
var flotData = [];
var circleFeature = null;
var iconFeature = null;


function loadMap() {
    console.log("Loading map");

    $('#link').html('<a href="sessions.html?device=' + device +'">Other activities</a>');

    $('.graph').hide();

    var point = new ol.geom.Point(ol.proj.transform([5.863839, 51.840291], 'EPSG:4326', 'EPSG:3857'));

    iconFeature = new ol.Feature(point);

    iconFeature.setStyle(new ol.style.Style({
        image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
            anchor: [125, 250   ],
            anchorXUnits: 'pixels',
            anchorYUnits: 'pixels',
            opacity: 0.75,
            scale: 0.35,
            src: '/images/marker.png'
        }))
    }));

    circleFeature = new ol.Feature(point);
    circleFeature.setStyle(new ol.style.Style({
        image: new ol.style.Circle({
            radius: 5,
            snapToPixel: false,
            fill: new ol.style.Fill({
                color: 'gray'
            }),
            stroke: new ol.style.Stroke({
                color: 'lightblue',
                width: 2
            })
        })
    }));

    map = new ol.Map({
        layers: [
            new ol.layer.Tile({
                source: new ol.source.OSM()
            }),
            new ol.layer.Vector({
                source: new ol.source.Vector({
                    features: [circleFeature]
                })
            }),
            new ol.layer.Vector({
                source: new ol.source.Vector({
                    features: [iconFeature]
                })
            })

        ],
        target: 'map',
        view: new ol.View({
            center: ol.proj.fromLonLat([5.863839, 51.840291]),
            zoom: 14
        })
    });

    var lineStyle = new ol.style.Style({
        // image: new ol.style.Circle({
        //     radius: 5,
        //     snapToPixel: false,
        //     fill: new ol.style.Fill({
        //         color: 'yellow'
        //     }),
        //     stroke: new ol.style.Stroke({
        //         color: 'red',
        //         width: 1
        //     })
        // }),
        stroke: new ol.style.Stroke({
            color: '#ff4433',
            width: 3
        })
    });

    var headStyle = new ol.style.Style({
        image: new ol.style.Circle({
            radius: 7,
            snapToPixel: false,
            fill: new ol.style.Fill({
                color: '#ff4433'
            })
        })
    });


    map.on('postcompose', function(event) {
        if (coordinates.length == 0) {
          $('#loading').fadeOut(2000);
          return;
        }
        var vectorContext = event.vectorContext;
        vectorContext.setStyle(lineStyle);
        var mp = new ol.geom.LineString(coordinates);
        mp.transform('EPSG:4326', 'EPSG:3857');
        vectorContext.drawGeometry(mp);

        var headPoint = new ol.geom.Point(coordinates[coordinates.length - 1]);
        headPoint.transform('EPSG:4326', 'EPSG:3857');

        vectorContext.setStyle(headStyle);
        vectorContext.drawGeometry(headPoint);
        $('#loading').fadeOut(2000);

    });

    map.render();
    $('.hint').click(function() {
        $('.hint').slideUp(400);
    });
    setTimeout(function(){
        $('.hint').slideUp(400);
    }, 10000);

}

function mapData(data) {
    var obj = { objtime: data.time };
    $.each(data.values, function() {
        obj[mapable(this.sensorid) + "_" + this.datatype] = this; //.value;
    });
    return obj;
}

function handleData(dataRecord, dataTable) {
    var hasLocation = false;
    //console.log(JSON.stringify(this));
    var time = new Date(dataRecord.time);
    var deviceId = dataRecord.device;
    //$('#info').html('Loaded data:' + time); //JSON.stringify(data));
    localData.push(mapData(dataRecord));
    if (localData.length > 2 && localData[localData.length - 1].objtime < localData[localData.length - 2].objtime) {
        localData.sort(function(a, b) {
            return a.objtime - b.objtime;
        });
    }
    $.each(dataRecord.values, function() {
        var datatype = this.datatype;
        if (datatype == 'location') {
            coordinates.push([this.value[1], this.value[0]]);
            coordinateTimes.push(time);
            hasLocation = true;
        }

        var sensorid = this.sensorid;
        var displaySensorDatatype = readable(this.datatype);
        var hoverSensorDatatype = readable(this.sensorid) + ", " + readable(this.datatype);
        var sensorDatatype = mapable(this.sensorid) + "_" + this.datatype;
        var foundIndex = -1;
        $.each(dataTable.rows, function(index) {
            if (
                this.cells[0].textContent == displaySensorDatatype) {
                foundIndex = index;
                return;
            }
        });

        var row = null;
        if (foundIndex >= 0) {
            row = dataTable.rows[foundIndex];
        } else {
            row = dataTable.insertRow(0);
            for (var i = 0; i < 3; i++) row.insertCell(i);
            if (dataTable.rows.length > 100) dataTable.deleteRow(100);
        }
        var dataAndUnit = humanizeDataAndUnit(this.value, datatype, this.unit)
        if (isNumber(this.value, datatype))
            row.cells[0].innerHTML = '<a href="#" id="graph_' + sensorDatatype+ '" title="'+ hoverSensorDatatype + '">' + displaySensorDatatype + '</a>';
        //onclick="loadGraph(\'' + datatype + '\',\'' + dataAndUnit[1] + '\')"
        else
            row.cells[0].innerHTML = displaySensorDatatype;
        row.cells[1].innerHTML = '<span id="' + sensorDatatype + '">' + dataAndUnit[0] + '</span>';
        row.cells[2].innerHTML = dataAndUnit[1];
        $('#graph_' + sensorDatatype).click(function(event) {
            loadGraph(sensorDatatype, hoverSensorDatatype, dataAndUnit[1]);
            event.stopPropagation();
        });
    });
    return hasLocation;
}


function loadData() {
    var url = "/api/data/" + device + '/' + session;
    //$('#info').html('Loading data:' + new Date()); //JSON.stringify(data));

    $('#map').click(function() {
        $('.data').toggle(500);
        $('#graph').hide(500);
    })

    $('.data').click(function(event) {
        $('.data').hide(500);
        //$('#graph').hide(500);
        event.stopPropagation();
    })

    $('#graph').click(function(event) {
        event.stopPropagation();
    })

    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        success: function(packet) {
            localData = [];
            coordinates = [];
            coordinateTimes = [];
            var dataTable = document.getElementById("dataTable");
            if (packet.result == "ok" && packet.data != null) {
                var hasLocations = false;
                packet.data.sort(function(a, b) {
                    return a.time - b.time;
                });
                $.each(packet.data, function(index) {
                    hasLocations |= handleData(this, dataTable);
                    //console.log(JSON.stringify(this));
                });
                if (hasLocations)  {
                    var newCenter = ol.proj.transform(coordinates[coordinates.length - 1], 'EPSG:4326', 'EPSG:3857');
                    map.getView().setCenter(newCenter);
                    circleFeature.getGeometry().setCoordinates(newCenter);
                    map.render();
                }
            }
        },
        error: function(xhr, status) {
            console.log('error getting Meterz data: ' + status);
        }
    });
}

function loadSocket() {
    socket.on('connect_message', function(data) {
        //$('#hint').html('Received connect message: ' + data.data);
        console.log('connect_message = ' + JSON.stringify(data));
        socket.emit('login', {
            name: "MeterzPage",
            deviceid: "B0:A2",
            room: "Plusot_" + device + "_" + session,
            timestamp: new Date().getTime()
        });
    });
    socket.on('update_broadcast', function(data) {
        //$('#hint').html('Received update broadcast: ' + JSON.stringify(data));
        console.log('update_broadcast = ' + JSON.stringify(data));

    });
    socket.on('joined', function(data) {
        //$('#hint').html('Received join: ' + JSON.stringify(data));
        console.log('joined = ' + JSON.stringify(data));
    });
    socket.on('login', function(data) {
        //$('#hint').html('Received login result: ' + JSON.stringify(data));
        console.log('login = ' + JSON.stringify(data));
    });
    socket.on('session', function(data) {
        //$('#hint').html('Received session info: <br>' + JSON.stringify(data));
        console.log('session = ' + JSON.stringify(data));
    });
    socket.on('content', function(data) {
        //$('#hint').html('Received data: ' + new Date(data.time)); //JSON.stringify(data));
        if (handleData(data, document.getElementById("dataTable")))  {
            var newCenter = ol.proj.transform(coordinates[coordinates.length - 1], 'EPSG:4326', 'EPSG:3857');
            map.getView().setCenter(newCenter);
            circleFeature.getGeometry().setCoordinates(newCenter);
            map.render();
        }
        //console.log('data = ' + JSON.stringify(data));

    });
    socket.on('time_broadcast', function(data) {
        // $('#hint').html('Received: time broadcast:' + new Date(data.timestamp));
        console.log('time_broadcast = ' + JSON.stringify(data));
    });
}

function updateGraphLegend() {
    updateLegendTimeout = null;
    if (plot == null) return;

    var pos = latestPosition;
    var axes = plot.getAxes();
    if (pos.x < axes.xaxis.min || pos.x > axes.xaxis.max ||
        pos.y < axes.yaxis.min || pos.y > axes.yaxis.max) return;

    var i, j; // dataset = plot.getData();
    for (i = 0; i < flotData.length; ++i) {
        var series = flotData[i];
        // find the nearest points, x-wise
        for (j = 0; j < series.data.length; ++j) if (series.data[j][0] > pos.x) break;

        // now interpolate
        var y,
            p1 = series.data[j - 1],
            p2 = series.data[j];

        if (p1 == null && p2 == null) continue;
        if (p1 == null)
            y = p2[1];
        else if (p2 == null)
            y = p1[1];
        else
            y = p1[1] + (p2[1] - p1[1]) * (pos.x - p1[0]) / (p2[0] - p1[0]);

        for (var type in localData[j]) {
            $('#' + type).html(humanizeDataAndUnit(localData[j][type].value, type, localData[j][type].unit)[0]);
        }
        //$('#' + series.datatype).text(humanizeDataAndUnit(y, series.datatype, series.unittype)[0]);
        $('#' + series.datatype).html(y.toFixed(1));

    }
    if (coordinateTimes != null) for (j = 0; j < coordinateTimes.length; ++j) if (coordinateTimes[j] > pos.x) {
        var newCenter = ol.proj.transform(coordinates[j], 'EPSG:4326', 'EPSG:3857');
        circleFeature.getGeometry().setCoordinates(newCenter);
        map.getView().setCenter(newCenter);
        break;
    }


}

function loadGraph(datatype, displayDatatype, unit) {
    flotData = [];
    var colors = ['#52a9f3', '#990000', '#996600', '#669900', '#ff9966', '#660099'];
    var index = 0;

    $('.graph').show();

    var data = localData.filter(function(elem, index, array) {
        return elem[datatype] != null;
    })

    data = data.map(function(d) {
        if (d[datatype] == null)
            return [new Date(d.objtime), humanScale(null, datatype)];
        else
            return [new Date(d.objtime), humanScale(d[datatype].value, datatype)];
    });

    flotData.push({
        color: colors[index],
        label: displayDatatype,
        lines: { show: true, lineWidth: 1, fill: true, fillColor: "rgba(" + hexToRgba(colors[index]) +",0.2)"  },
        data: data,
        datatype: datatype,
        unittype: unit
    });

    var options =
        {
            //            grid: {
            //                borderWidth: 1,
            //                minBorderMargin: 20,
            //                labelMargin: 10,
            //                backgroundColor: {
            //                    colors: ["#fff", "#e4f4f4"]
            //                },
            //                margin: {
            //                    top: 8,
            //                    bottom: 20,
            //                    left: 20
            //                }
            //            },
            series:
            {
                points: { show: true },
                bars: { show: false }
            },
            xaxis:
            {
                label: 'time',
                mode: 'time',
                timezone: "browser",
                monthNames: ["jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"],
                dayNames: ["sun", "mon", "tue", "wed", "thu", "fri", "sat"],
                timeformat: '%H:%M',
                ticks: 4
            },
            yaxis:
            {
                //                label: datatype
                //                {tickFormatter: function (x) {
                //                                      return x.toString().replace(/\B(?=(?:\d{3})+(?!\d))/g, ",");
                //                                }
            },
            legend: {
                position: 'nw'
            },
            grid: {
                hoverable: true,
                autoHighlight: true,
                backgroundColor: {
                    colors: ["#fff", "#dfdee3"]
                }
            },
            crosshair: {
                mode: "x"
            },
            selection: {
                mode: "x"
            }

        };

    var container = $('#graph');
    plot = $.plot(container, flotData, options);

    container.bind("plothover", function (event, pos, item) {
        latestPosition = pos;
        if (!updateLegendTimeout)
            updateLegendTimeout = setTimeout(updateGraphLegend, 50);
    });

    container.bind("plotselected", function (event, ranges) {
        $.each(plot.getXAxes(), function(_, axis) {
            var opts = axis.options;
            opts.min = ranges.xaxis.from;
            opts.max = ranges.xaxis.to;
        });
        plot.setupGrid();
        plot.draw();
        plot.clearSelection();

    });

    //    var container = $('#graph');
    //    var yaxisLabel = $("<div class='axisLabel yaxisLabel'></div>")
    //		.text(datatype)
    //		.appendTo(container);

    // Since CSS transforms use the top-left corner of the label as the transform origin,
    // we need to center the y-axis label by shifting it down by half its width.
    // Subtract 20 to factor the chart's bottom margin into the centering.

    //	yaxisLabel.css("margin-top", yaxisLabel.width() / 2 - 20);


    // flotCanvas = plot.getCanvas();



    //    function savePlot() {
    //        if (flotCanvas == null) return;
    //        //flotCanvas.toDataURL("image/png");
    //        var image = flotCanvas.toDataURL("image/png").replace("image/png", "image/octet-stream");  // here is the most important part because if you dont replace you will get a DOM 18 exception.
    //        //window.location.href=image;
    //        var dateStr = new Date().toLocaleDateString();
    //        $('<a download="grafiek ' + dateStr + '.png" href="' + image + '"></a>')[0].click()
    //    }



}
