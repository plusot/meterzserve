var device = $.getUrlVar('device');

function load() {
    var url = "/api/sessions/" + device;
    $('#info').html("Activities for " + device);
    $.ajax(
        {
            url : url,
            type : 'GET',
            dataType : 'json',
            success: function(packet) {
                var dataTable = document.getElementById("dataTable");
                if (packet.result == "ok" && packet.data != null) {
                    packet.data.sort(function(a,b)  {
                        return a.session - b.session;
                    });
                    $.each(packet.data, function(index) {
                        console.log(JSON.stringify(this));
                        var newRow = dataTable.insertRow(1);
                        var col3Str = "";
                        var col4Str = "";
                        if (this.data != null) $.each(this.data.values, function() {
                            var dataAndUnit = humanizeDataAndUnit(this.value, this.datatype, this.unit);
                            if (col4Str.length > 0) {
                                col3Str += "<br>";
                                col4Str += "<br>";
                            }
                            col3Str += readable(this.sensorid) + ", " + readable(this.datatype);
                            col4Str += dataAndUnit[0];
                            if (dataAndUnit.length > 1 && dataAndUnit[1].length > 0) col4Str += " " + dataAndUnit[1];
                        });

                        newRow.insertCell(0).innerHTML = '<a href="/map.html?device=' + device + '&session=' + this.session + '">' +  
                            this.session.substring(4,6) + "-" + this.session.substring(2,4) + "-20" + this.session.substring(0,2) + '</a>';
                        newRow.insertCell(1).innerHTML = col3Str;
                        newRow.insertCell(2).innerHTML = col4Str;
                        //newRow.insertCell(3).innerHTML = '<a href="/map.html?device=' + device + '&session=' + this.session + '&replace=1">Map</a>';
                        if (dataTable.rows.length > 100) dataTable.deleteRow(100);
                    });
                }
            },
            error: function(xhr, status) {
                console.log('error getting Meterz data: ' + status);
            }
        }
    );
}

