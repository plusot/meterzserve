var socket = io();
var replace = $.getUrlVar('replace');
var device = $.getUrlVar('device');
var session = $.getUrlVar('session');

function load() {
    var url = "/api/data/" + device + '/' + session;
    $('#info').html('Loading data:' + new Date()); //JSON.stringify(data));
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        success: function(packet) {
            var dataTable = document.getElementById("dataTable");
            if (packet.result == "ok" && packet.data != null) {
                packet.data.sort(function(a, b) {
                    return a.time - b.time;
                });
                $.each(packet.data, function(index) {
                    //console.log(JSON.stringify(this));
                    var time = time2str(new Date(this.time));
                    var deviceId = this.device;
                    $('#info').html('Loaded data:' + time); //JSON.stringify(data));
                    $.each(this.values, function() {
                        var sensorid = this.sensorid;
                        var datatype = this.datatype;
                        var foundIndex = 0;
                        if (replace) {
                            $.each(dataTable.rows, function(index) {
                                if (
                                    this.cells[0].textContent == sensorid &&
                                    this.cells[1].textContent == datatype) {
                                    foundIndex = index;
                                    return;
                                }
                            });
                        }
                        var row = null;
                        if (foundIndex >= 1) {
                            row = dataTable.rows[foundIndex];
                            row.cells[0].innerHTML = sensorid;
                            row.cells[1].innerHTML = datatype;
                            row.cells[2].innerHTML = humanize(this.value, datatype);
                            row.cells[3].innerHTML = this.unit;
                            //row.cells[4].innerHTML = this.class;
                            row.cells[4].innerHTML = time;
                        } else {
                            var newRow = dataTable.insertRow(1);
                            newRow.insertCell(0).innerHTML = sensorid;
                            newRow.insertCell(1).innerHTML = datatype;
                            newRow.insertCell(2).innerHTML = humanize(this.value, datatype);
                            newRow.insertCell(3).innerHTML = this.unit;
                            //newRow.insertCell(4).innerHTML = this.class;
                            newRow.insertCell(4).innerHTML = time;
                            if (dataTable.rows.length > 100) dataTable.deleteRow(100);
                        }
                    });

                });
            }
        },
        error: function(xhr, status) {
            console.log('error getting Meterz data: ' + status);
        }
    });
}

function loadSocket() {
    socket.on('connect_message', function(data) {
        $('#info').html('Received connect message: ' + data.data);
        console.log('connect_message = ' + JSON.stringify(data));
        socket.emit('login', {
            name: "MeterzPage",
            deviceid: "B0:A2",
            room: "Plusot_" + device + "_" + session,
            timestamp: new Date().getTime()
        });
    });
    socket.on('update_broadcast', function(data) {
        $('#info').html('Received update broadcast: ' + JSON.stringify(data));
        console.log('update_broadcast = ' + JSON.stringify(data));

    });
    socket.on('joined', function(data) {
        $('#info').html('Received join: ' + JSON.stringify(data));
        console.log('joined = ' + JSON.stringify(data));
    });
    socket.on('login', function(data) {
        $('#info').html('Received login result: ' + JSON.stringify(data));
        console.log('login = ' + JSON.stringify(data));
    });
    socket.on('session', function(data) {
        $('#info').html('Received session info: ' + JSON.stringify(data));
        console.log('session = ' + JSON.stringify(data));
    });
    socket.on('content', function(data) {
        $('#info').html('Received data: ' + new Date(data.time)); //JSON.stringify(data));
        var dataTable = document.getElementById("dataTable"); //$('#dataTable');
        $.each(data.values, function() {
            var sensorid = this.sensorid;
            var datatype = this.datatype;
            var foundIndex = 0;
            if (replace) {
                $.each(dataTable.rows, function(index) {
                    if (
                        this.cells[0].textContent == sensorid &&
                        this.cells[1].textContent == datatype) {
                        foundIndex = index;
                        return;
                    }
                });
            }
            var row = null;
            if (foundIndex >= 1) {
                row = dataTable.rows[foundIndex];
                row.cells[0].innerHTML = sensorid;
                row.cells[1].innerHTML = datatype;
                row.cells[2].innerHTML = humanize(this.value, datatype);
                row.cells[3].innerHTML = this.unit;
                //row.cells[4].innerHTML = this.class;
                row.cells[4].innerHTML = time2str(new Date(data.time));
            } else {
                row = dataTable.insertRow(1);
                row.insertCell(0).innerHTML = sensorid;
                row.insertCell(1).innerHTML = datatype;
                row.insertCell(2).innerHTML = humanize(this.value, datatype);
                row.insertCell(3).innerHTML = this.unit;
                //row.insertCell(4).innerHTML = this.class;
                row.insertCell(4).innerHTML = time2str(new Date(data.time));
            }
            if (dataTable.rows.length > 100) dataTable.deleteRow(100);
        });
        //console.log('data = ' + JSON.stringify(data));

    });
    socket.on('time_broadcast', function(data) {
        $('#info').html('Received: time broadcast:' + new Date(data.timestamp));
        console.log('time_broadcast = ' + JSON.stringify(data));
    });
}