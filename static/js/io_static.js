function load() {
    var device = $.getUrlVar('device');
    var session = $.getUrlVar('session');
    var url = "/api/data/" + device + '/' + session;
    $.ajax(
        {
            url : url,
            type : 'GET',
            dataType : 'json',
            success: function(packet) {
                var dataTable = document.getElementById("dataTable");
                if (packet.result == "ok" && packet.data != null) {
                    packet.data.sort(function(a,b)  {
                        return a.time - b.time;
                    });
                    $.each(packet.data, function(index) {
                        console.log(JSON.stringify(this));
                        var time = time2str(new Date(this.time));
                        var deviceId = this.device;
                        $('#info').html('Received data:' + time); //JSON.stringify(data));
                        $.each(this.data, function() {
                            var newRow = dataTable.insertRow(1);
                            newRow.insertCell(0).innerHTML = deviceId;
                            newRow.insertCell(1).innerHTML = this.sensorid;
                            newRow.insertCell(2).innerHTML = this.datatype;
                            newRow.insertCell(3).innerHTML = humanize(this.value);
                            newRow.insertCell(4).innerHTML = this.unit;
                            newRow.insertCell(5).innerHTML = this.class;
                            newRow.insertCell(6).innerHTML = time;
                            //console.log(data);
                            if (dataTable.rows.length > 100) dataTable.deleteRow(100);
                        });

                    });
                }
            },
            error: function(xhr, status) {
                console.log('error getting Meterz data: ' + status);
            }
        }
    );
}

load();


//var socket = io();
//socket.on('connect_message', function (data) {
//    $('#info').html('Received connect message:' + data.data);
//    console.log(data);
//    socket.emit('login', { name: "Soapie", deviceid: "B0:A2", room: "Plusot", timestamp: new Date().getTime() });
//});
//socket.on('update_broadcast', function (data) {
//    $('#info').html('Received update broadcast: ' + JSON.stringify(data));
//    console.log(data);
//});
//socket.on('joined', function (data) {
//    $('#info').html('Received join: ' + JSON.stringify(data));
//    console.log(data);
//});
//socket.on('data', function (data) {
//    $('#info').html('Received data:' + new Date(data.data.time)); //JSON.stringify(data));
//    var dataTable = document.getElementById("dataTable"); //$('#dataTable');
//    var newRow = dataTable.insertRow(1);
//    newRow.insertCell(0).innerHTML = data.deviceid;
//    newRow.insertCell(1).innerHTML = data.sensorid;
//    newRow.insertCell(2).innerHTML = data.data.datatype;
//    newRow.insertCell(3).innerHTML = humanize(data.data.value);
//    newRow.insertCell(4).innerHTML = data.data.unit;
//    newRow.insertCell(5).innerHTML = data.data.class;
//    newRow.insertCell(6).innerHTML = new Date(data.data.time);
//    console.log(data);
//    if (dataTable.rows.length > 100) dataTable.deleteRow(200);
//});
//socket.on('time_broadcast', function (data) {
//    $('#info').html('Received: time broadcast:' + new Date(data.timestamp));
//    console.log(data);
//});
