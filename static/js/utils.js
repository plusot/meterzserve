function constrain(v, min, max){
    if( v < min )
        v = min;
    else
        if( v > max )
            v = max;
    return v;
}

function screenXY(vec3){
    //var projector = new THREE.Projector();
    //var vector = projector.projectVector( vec3.clone(), camera );
    var vector = vec3.project(camera)
    var result = {};
    var windowWidth = window.innerWidth;
    var minWidth = 1280;
    if(windowWidth < minWidth) {
        windowWidth = minWidth;
    }
    result.x = Math.round( vector.x * (windowWidth/2) ) + windowWidth/2;
    result.y = Math.round( (0-vector.y) * (window.innerHeight/2) ) + window.innerHeight/2;
    return result;
}

function pad(number, digits) {
    number = Math.floor(number);
    return Array(Math.max(digits - String(number).length + 1, 0)).join(0) + number;
}

function time2str(time, isUTC) {
    var t = new Date(time);
    if (isUTC) {
        var str = t.getUTCHours() + ':';
        if (t.getUTCMinutes() < 10)
            str += '0' + t.getUTCMinutes() + ':';
        else
            str += t.getUTCMinutes() + ':';
        if (t.getUTCSeconds() < 10) return str + '0' + t.getUTCSeconds();
        return str + t.getUTCSeconds();
    }
    var str = t.getHours() + ':';
    if (t.getMinutes() < 10)
        str += '0' + t.getMinutes() + ':';
    else
        str += t.getMinutes() + ':';
    if (t.getSeconds() < 10) return str + '0' + t.getSeconds();
    return str + t.getSeconds();
}

function milli2str(time) {

    var hours = Math.floor(time / 3600000);
    var rest = time % 3600000;
    var minutes = Math.floor(rest / 60000);
    rest %= 60000;
    var seconds = Math.floor(rest / 1000);
    return pad(hours, 2) + ':' + pad(minutes,2) + ':' + pad(seconds,2);
}

function datetime2str(time, br) {
    var t = new Date(time);
    var str = t.getUTCFullYear() + '-';
    if (t.getUTCMonth() < 10)
        str += '0' + t.getUTCMonth() + '-';
    else
        str += t.getUTCMonth() + '-';
    if (t.getUTCDate() < 10)
        str += '0' + t.getUTCDate();
    else
        str += t.getUTCDate();
    if (br) str += br; else str += " ";
    str += t.getUTCHours() + ':';
    if (t.getUTCMinutes() < 10)
        str += '0' + t.getUTCMinutes() + ':';
    else
        str += t.getUTCMinutes() + ':';
    if (t.getUTCSeconds() < 10) return str + '0' + t.getUTCSeconds();
    return str + t.getUTCSeconds();
}

function humanize(x, datatype){
    if (!x) return x;
    if (Array.isArray(x)) {
        var str = '';
        for (var i in x) {
            if (str.length > 0) str += ', ';
            str += humanize(x[i]);
        }
        return str;
    }
    if (datatype != null) {
        var y = x;
        if (datatype.includes("time_active") || datatype.includes("time_activity") || datatype.includes("time_total") || datatype.includes("time_moving")) {
            return milli2str(y);
        } else if (datatype.includes("time")) {
            return time2str(y);
        } else if (datatype.includes("speed")) {
            y *= 3.6;
            return y.toFixed(1);
        } else if (datatype.includes("distance")) {
            y /= 1000;
            return y.toFixed(1);
        } else if (datatype.includes("cpu")) {
            y *= 100; 
            return y.toFixed(1);
        } else if (datatype.includes("humidity")) {
            y *= 100; 
            return y.toFixed(1);
        } else if (datatype.includes("memory")) {
            if (y > 1000000) y /= 1000000;
            return y.toFixed(1);
        } else if (datatype.includes("slope")) {
            y *= 100; 
            return y.toFixed(1);
        } else if (datatype.includes("battery")) {
            y *= 100; 
            return y.toFixed(1);
        } else if (datatype.includes("air_pressure")) {
            if (y > 2000.0) y /= 100.0;
            return y.toFixed(1);
        } else if (datatype.includes("pressure")) {
            if (y > 2000.0) y /= 100.0;
            return y.toFixed(1);
        } else if (datatype.includes("location")) {
            return y.toFixed(4);
        }
    }
    if (typeof x.toFixed == 'function')
        return x.toFixed(1);
    else
        return x;
}

function humanizeDataAndUnit(x, datatype, unit){
    //if (typeof x.toFixed != 'function') return [x, unit];
    if (x == null) {
//        console.log('x == null');
        return [x, unit];
    }
    if (Array.isArray(x)) {
        var str = '';
        for (var i in x) {
            if (str.length > 0) str += '<br>';
            str += humanize(x[i], datatype);
        }
        return [str, unit];
    } else if (datatype != null) {
        var y = x;
        if (datatype.includes("time_active") || datatype.includes("time_activity") || datatype.includes("time_total") || datatype.includes("time_moving")) {
            return [milli2str(y), ""];
        } else if (datatype.includes("time")) {
            return [time2str(y), ""];
        } else if (datatype.includes("air_pressure")) {
            if (y > 2000) y /= 100.0;
            return [y.toFixed(1), 'mbar'];
        } else if (datatype.includes("pressure")) {
            if (y > 2000) y /= 100.0;
            return [y.toFixed(1), 'mbar'];
        } else if (datatype.includes("cpu")) {
            y *= 100; 
            return [y.toFixed(1), '&#37;'];
        } else if (datatype.includes("humidity")) {
            y *= 100; 
            return [y.toFixed(1), '&#37;'];
        } else if (datatype.includes("memory")) {
            y /= 1000000;
            return [y.toFixed(1), 'MB'];
        } else if (datatype.includes("battery")) {
            y *= 100; 
            return [y.toFixed(1), '&#37;'];
        } else if (datatype.includes("slope")) {
            y *= 100; 
            return [y.toFixed(1), '&#37;'];
        } else if (datatype.includes("speed") && unit == "m/s") {
            y *= 3.6;
            return [y.toFixed(1), 'km/h'];
        } else if (datatype.includes("distance") && unit == "m") {
            y /= 1000.0;
            return [y.toFixed(1), 'km'];
        } else if (unit === "l" && y < 1.0) {
            y *= 1000.0;
            return [y.toFixed(1), 'ml'];
        } 
    } 
    if (typeof x.toFixed == 'function')
        return [x.toFixed(1), unit];
    else
        return ["" + x, unit];
}

function humanScale(x, datatype){
    if (!x) return x;
    if (Array.isArray(x)) {
        var parts = [];
        for (var i in x) {
            parts.push(humanScale(x[i]));
        }
        return parts;
    } else if (datatype != null) {
        if (datatype.includes("speed")) {
            return x * 3.6;  
        } else if (datatype.includes("distance")) {
            return x / 1000.0;
        } else if (datatype.includes("air_pressure")) {
            if (x > 2000) return x /= 100.0;
        } else if (datatype.includes("pressure")) {
            if (x > 2000) return x /= 100.0;
        } else if (datatype.includes("memory")) {
            return x / 1000000;
        } else if (datatype.includes("slope")) {
            return x * 100;
        } else if (datatype.includes("humidity")) {
            return x * 100; 
        } else if (datatype.includes("cpu")) {
            return x * 100; 
        } else if (datatype.includes("battery")) {
            return x * 100; 
        } 
    } 
    return x;
}

function isNumber(x, datatype){
    if (!$.isNumeric(x)) return false;
    if (Array.isArray(x)) {
        return false;
    }
    if (datatype != null && datatype.includes("operating_time")) {
        return false;
    }
    if (datatype != null && datatype.includes("time")) {
        return false;
    }
    if (typeof x.toFixed == 'function')
        return true;
    else
        return true;
}

function hexToRgba(hex) {
    var bigint = parseInt(hex.substring(1), 16);
    var r = (bigint >> 16) & 255;
    var g = (bigint >> 8) & 255;
    var b = bigint & 255;

    return r + "," + g + ", " + b;
}

function mapable(member) {
    return member.split("+").join("").split(".").join("_").split(" ").join("_");
}

function readable(member) {
    return member.split("_").join(" ").replace(" Sensor", "").replace(" level", "");
}

$.extend({
    getUrlVars: function(){
        var vars = [];
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        $.each(hashes, function() {
            var item = this.split('=');
            vars[item[0]] = item[1];
        });
        return vars;
    },
    getUrlVar: function(name){
        return $.getUrlVars()[name];
    }
});

String.prototype.toProperCase = function() {
    var separators = [' ', '_', '\\\+', '-', '\\\(', '\\\)', '\\*', ':', '\\\?'];
    var strs = this.replace('soapsense_', '').replace('_p_', ' / ').split(new RegExp(separators.join('|'), 'g'));

    strs = strs.map(function(i){
        return i[0].toUpperCase() + i.substring(1)
    });
    var str = strs.join(" ");
    return str;
}