function getValue(data, type) {
    $.each(data, function() {
        if (this.data_type == type) return this.value;
    });
    return null;
}

function getValues(data) {
    var result = {};
    $.each(data, function() {
        result[this.datatype] = this.value;
    });
    return result;
}

function load(full) {
    var url = "/api/devices";
    $.ajax(
        {
            url : url,
            type : 'GET',
            dataType : 'json',
            success: function(packet) {
                var dataTable = document.getElementById("dataTable");
                if (packet.result == "ok" && packet.data != null) {
                    packet.data.sort(function(a,b)  {
                        return a.device - b.device;
                    });
                    $.each(packet.data, function(index) {
                        if (this.data != null) {
                            var date = new Date(this.time);
                            var info = getValues(this.data);
                            if (info["device_name"] != null) {
                                var newRow = dataTable.insertRow(1);
                                var battery = info["battery_level"];
                                if (battery <= 1) battery *= 100;
                                var index = 0;
                                newRow.insertCell(index++).innerHTML = '<a href="/sessions.html?device=' + this.device + '">' + this.device.slice(-4) + '</a>'; newRow.insertCell(index++).innerHTML = info["device_name"] + '<br>' + info["hardware_revision_string"];
                                newRow.insertCell(index++).innerHTML = info["software_revision_string"] + '<br>' + info["android_version"];
//                                if (full != null) 
//                                    newRow.insertCell(index++).innerHTML = '<a href="mailto:' + info["email_address"] + '?subject=Meterz" target="_blank">' + info["user_name"] + '</a>';
//                                else
//                                    newRow.insertCell(index++).innerHTML = info["user_name"];
                                newRow.insertCell(index++).innerHTML = Math.round(battery) + '&#37;';

                                newRow.insertCell(index++).innerHTML = date.toLocaleDateString()  + '<br>' + date.toLocaleTimeString();
                                if (dataTable.rows.length > 100) dataTable.deleteRow(100);
                            }
                        }
                    });
                }
            },
            error: function(xhr, status) {
                console.log('error getting Meterz data: ' + status);
            }
        }
    );
}

//load();