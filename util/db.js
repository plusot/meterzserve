var mongo = require('mongodb').MongoClient,
    util = require('util'),
    nedb = require('nedb'),
    async = require('async'),
    _ = require('underscore'),

    db = {},
    isNetDB = false,
    nedbPath = 'data/nedb/store';


function addNedbCollection(collection, callback) {
    db[collection] = new nedb({ filename: nedbPath + '_' + collection + '.db' });
    db[collection].loadDatabase(function (err) {
        if (err) {
            console.log("Could not connect to Nedb: " + err);
            if (callback != null) callback(err, 'Could not connect to NeDB: ' + collection);
        } else {
            console.log("Connected to NeDB: " + nedbPath + " collection: " + collection);
            isNetDB = true;
            if (callback != null) callback(null, 'NeDB connected: ' + collection);
        }
    });
    if (collection.startsWith('devices'))
        db[collection].ensureIndex({ fieldName: 'device', unique: true }, function (err) {
            if (err) console.log("Could not add index to " + collection);
        });
    else if (collection.startsWith('sessions'))
        db[collection].ensureIndex({ fieldName: 'session', unique: true }, function (err) {
            if (err) console.log("Could not add index to " + collection);
        });
};

module.exports = {
    isNedb : function() {
        return isNetDB;
    },
    connect : function (use_mongo, callback) {
        if (use_mongo) {
            var url = 'mongodb://localhost:27017/meterz';
            mongo.connect(url, function(err, localDb) {
                if (err) {
                    console.log("Could not connect to Mongo: " + err);
                    db = {};
                    addNedbCollection('data', callback);
                    return;
                }
                console.log("Connected to Mongo");
                db = localDb;
                if (callback != null) callback(null, 'MongoDB connected');
                //db.close();
            });
        } else {
            isNetDB = true;
            console.log("Using to NeDB");
        }
    },
    insertDoc : function(collection, data, callback) {
        if (db == null) {
            console.log("Could not insert data in " + collection);
            if (callback != null) callback(new Error("no db"));
            return;
        }
        if (isNetDB) {
            async.series({
                check: function(asyncCallback) {
                    if (db[collection] == null) {
                        addNedbCollection(collection, asyncCallback);
                    } else
                        asyncCallback(null, 'Collection "' + collection + '" exists');
                },
                insert: function(asyncCallback) {
                    db[collection].insert(data, function (err, newDocs) {
                        if (err) {
                            console.log("Could not insert data in " + collection);
                            asyncCallback(err);
                            return;
                        }
                        //console.log("Inserted " + util.inspect(newDocs) );
                        asyncCallback(null, {docs: newDocs, total: newDocs.length != null ? newDocs.length : 1});
                    });
                }
            }, function(err, results) {
                if (err) {
                    if (callback != null) callback(err, 'Error in async function insertDoc');
                    return;
                } else {
                    //console.log('Results of async operation: ' + util.inspect(results));
                    if (callback != null) callback(null, results.insert);
                }
            });
        } else {
            db.collection(collection).insertOne(data, function(err, result) {
                if (err) {
                    console.log("Could not insert data in " + collection);
                    if (callback != null) callback(err);
                    return;
                }
                console.log("Inserted " + util.inspect(data) + " in " + collection);
                if (callback != null) callback(null, result, 1);
            });
        }
    },
    updateDoc : function(collection, query, data, callback) {
        if (db == null) {
            console.log("Could not update data in " + collection);
            if (callback != null) callback(new Error("no db"), 'No DB');
            return;
        }
        if (isNetDB) {
            var options = { upsert:true };
            async.series({
                check: function(asyncCallback) {
                    if (db[collection] == null) {
                        addNedbCollection(collection, asyncCallback);
                    } else
                        asyncCallback(null, 'Collection "' + collection + '" exists');
                },
                update: function(asyncCallback) {
                    db[collection].update(query, data, options, function (err, numReplaced, upserted) {
                        if (err) {
                            console.log("Could not update data in " + collection + ":"  + util.inspect(err));
                            asyncCallback(err);
                            return;
                        }
                        //console.log("Inserted " + util.inspect(newDocs) );
                        asyncCallback(null, { replaced: numReplaced, upserted: upserted });
                    });
                }
            }, function(err, results) {
                if (err) {
                    if (callback != null) callback(err, 'Error in async function updateDoc');
                    return;
                } else {
                    //console.log('Results of async operation: ' + util.inspect(results));
                    if (callback != null) callback(null, results.update);
                }
            });
        } else {
            if (callback != null) callback(new Error("Not implemented yet"), "Mongo 'update' not yet implemented");

        }
    },
    findDocs : function(collection, fieldValues, callback) {
        if (db == null) {
            console.log("Could not find data in " + collection);
            if (callback != null) callback(new Error("no db"));
            return;
        }
        if (isNetDB) {
            async.series({
                check: function(asyncCallback) {
                    if (db[collection] == null) {
                        addNedbCollection(collection, asyncCallback);
                    } else
                        asyncCallback(null, 'Collection "' + collection + '" exists');
                },
                result: function(asyncCallback) {
                    db[collection].find(fieldValues, function (err, docs) {
                        if (err) {
                            console.log("Could not find data in " + collection + ", error: " + err);
                            asyncCallback(err);
                            return;
                        }
                        //console.log("Found " + util.inspect(docs) );
                        asyncCallback(null, docs);
                    });
                }
            }, function(err, results) {
                if (err) {
                    if (callback != null) callback(err, 'Error in async function findDocs');
                    return;
                } else {
                    //console.log('Results of async operation: ' + results);
                    if (callback != null) callback(null, results.result);
                }
            });
        } else {
            var cursor = db.collection(collection).find(fieldValues);
            if (fieldValues != null) console.dir(fieldValues);
            var docs = [];
            cursor.each(function(err, doc) {
                if (err) {
                    console.log("Could not find data in " + collection);
                    if (callback != null) callback(err);
                    return;
                }
                if (doc != null) {
                    //console.dir(doc);
                    docs.push(doc);
                } else if (callback != null) {
                    callback(null, docs);
                }
            });
        }
    },

    countDocs : function(collection, field, callback) {
        if (db == null) {
            console.log("Could not aggregate data from " + collection);
            if (callback != null) callback(new Error("no db"));
            return;
        }
        if (isNetDB) {
            async.series({
                check: function(asyncCallback) {
                    if (db[collection] == null) {
                        addNedbCollection(collection, asyncCallback);
                    } else
                        asyncCallback(null, 'Collection "' + collection + '" exists');
                },
                result: function(asyncCallback) {
                    db[collection].count(field, function (err, docs) {
                        if (err) {
                            console.log("Could not count data in " + collection + ", error: " + err);
                            asyncCallback(err);
                            return;
                        }
                        //console.log("Counted " + util.inspect(docs) );
                        asyncCallback(null, docs);
                    });
                }
            }, function(err, results) {
                if (err) {
                    if (callback != null) callback(err, 'Error in async function countDocs');
                    return;
                } else {
                    //console.log('Results of async operation: ' + results);
                    if (callback != null) callback(null, results.result);
                }
            });
        } else {
            db.collection(collection).aggregate(
                [
                    { $group: { "_id": "$" + field, "count": { $sum: 1 } } }
                ]).toArray(function(err, result) {
                if (err) {
                    console.log("Could not aggregate data from " + collection + " for " + field);
                    if (callback != null) callback(err);
                    return;
                }
                console.dir(result);
                callback(null, result);
            });
        }
    },

    aggregate : function(collection, aggregator, callback) {
        if (callback == null) {
            console.log('aggregate function needs callback');
            return;
        }
        if (db == null) {
            console.log("Could not aggregate data from " + collection);
            callback(new Error("no db"));
            return;
        }
        if (isNetDB) {
            async.series({
                check: function(asyncCallback) {
                    if (db[collection] == null) {
                        addNedbCollection(collection, asyncCallback);
                    } else
                        asyncCallback(null, 'Collection "' + collection + '" exists');
                },
                find: function(asyncCallback) {
                    db[collection].find({}, function (err, docs) {
                        if (err) {
                            console.log("Could not find data in " + collection + ", error: " + err);
                            asyncCallback(err);
                            return;
                        }
                        //console.log("Aggregating " + util.inspect(docs) );
                        asyncCallback(null, docs);
                    });
                }
            }, function(err, results) {
                if (err) {
                    if (callback != null) callback(err, 'Error in async function aggregate');
                    return;
                } else {
                    //console.log('Results of async operation: ' + results);
                    if (results.find != null && results.find.length > 0) {
                        callback(null, _.countBy(results.find, aggregator));
                    } else
                        callback(null, {});
                }
            });
        } else {
            db.collection(collection).aggregate(aggregator).toArray(function(err, result) {
                if (err) {
                    console.log("Could not aggregate data from " + collection + " with " + util.inspect(aggregator));
                    callback(err);
                    return;
                }
                console.dir(result);
                callback(null, result);
            });
        }
    },

    removeDocs : function(collection, query, callback) {
        if (db == null) {
            console.log("Could not remove data from " + collection);
            if (callback != null) callback(new Error("no db"));
            return;
        }
        if (isNetDB) {
            async.series({
                check: function(asyncCallback) {
                    if (db[collection] == null) {
                        addNedbCollection(collection, asyncCallback);
                    } else
                        asyncCallback(null, 'Collection "' + collection + '" exists');
                },
                remove: function(asyncCallback) {
                    db[collection].remove(query, { multi: true }, function (err, numRemoved) {
                        if (err) {
                            console.log("Could not remove data from " + collection + ", error: " + err);
                            asyncCallback(err);
                            return;
                        }
                        //console.log("Removed " + numRemoved + " docs");
                        asyncCallback(null, numRemoved);
                    });
                }
            }, function(err, results) {
                if (err) {
                    if (callback != null) callback(err, 'Error in async function removeDocs');
                } else {
                    //console.log('Results of async operation: ' + results);
                    callback(null, results.remove);
                }
            });
        } else {
            db.collection(collection).deleteMany( query, function(err, results) {
                if (err) {
                    console.log("Could not remove data from " + collection);
                    if (callback != null) callback(err);
                } else {
                    console.log(results);
                    callback();
                }
            });
        }
    },
    close : function(callback) {
        if (db == null) return;
        if (isNetDB) {
            //            for (var collection in db) {
            //                db.collection.close();
            //            }
            if (callback != null) callback(null, 'NeDB Database does not need to be closed');
        } else {
            db.close();
            if (callback != null) callback(null, 'Mongo Database closed');
        }

    }
}