// tools.js
// ========
module.exports = {
    server_port : 6002,
    ssl_server_port : 6443,
    use_mongo : false
};
