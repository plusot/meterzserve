/**
 * Modified by peet on 19-09-16.
 */

var express = require('express');
var async = require('async');
var app = express();
var http = require('http').Server(app);
var https = require('https');
var crypto = require('crypto');
var fs = require('fs');
var path = require('path');
var bodyParser = require('body-parser');
var io = require('socket.io')(http);
var util = require('util');
var serveIndex = require('serve-index');
var _ = require('underscore');

var tools = require('./util/tools');
var dbs = require('./util/db');
var gl = require('./globals');

var db = null;
var staticSensors = {};

var sslOptions = {
    key: fs.readFileSync('ssl/privkey.pem'),
    cert: fs.readFileSync('ssl/fullchain.pem')
};

function handleError(res, err) {
    if (err.message != null)
        tools.send_data(res, { result: "error", info: err.message});
    else
        tools.send_data(res, { result: "error", info: err});
}

function isCorrect(err, value, res) {
    if (err) {
        handleError(res, err.message);
    } else if (value == null) {
        tools.send_data(res, { result: "error", info: "no data"});
    } else
        return true;
    return false;
}

app.use(express.static(__dirname + "/static"));
app.use("/apps", serveIndex(__dirname + "/static/apps"));
app.use("/files", express.static(__dirname + "/files"));

app.use(function(req, res, next) {
    var contentType = req.headers['content-type'] || '';
    var mime = contentType.split(';')[0];

    if (mime != 'text/plain') {
        return next();
    }

    var data='';
    req.setEncoding('utf8');
    req.on('data', function(chunk) {
        data += chunk;
    });

    req.on('end', function() {
        req.raw = data;
        next();
    });
});
app.use(bodyParser.json());

app.get('/hello', function (req, res) {
    tools.send_data(res, {hello: "Meterz Service API", now: tools.fileDateLong()});
});

app.get('/api/devices', function(req, res) {
    dbs.findDocs('devices', null, function(err, docs) {
        if (isCorrect(err, docs, res)) {
            //console.log("Lookup done");
            tools.send_data(res, { result: "ok", data : docs });
        }
    });
});

app.get('/api/sensor/:device/:field', function(req, res) {
    var device = req.params.device;
    var field = req.params.field;

    console.log("Request device " + field + " for: " + device + " " + JSON.stringify(staticSensors[device]));
    if (staticSensors[device] == null || staticSensors[device].info[field] == null)
        tools.send_data(res, { result: "error", info: "device not available" } );
    else {
        var obj = {};
        obj[field] = staticSensors[device].info[field];
        tools.send_data(res, obj);
    }
});

app.get('/api/sensor/:device', function(req, res) {
    var device = req.params.device;

    console.log("Request device info for: " + device);
    if (staticSensors[device] == null)
        tools.send_data(res, { result: "error", info: "device not available" } );
    else
        tools.send_data(res, { value: staticSensors[device]});
});

app.get('/api/sessions/:device', function(req, res) {
    var device = req.params.device;
    dbs.findDocs('sessions_' + device, null, function(err, docs) {
        if (isCorrect(err, docs, res)) {
            //console.log("Lookup done");
            tools.send_data(res, { result: "ok", data : docs });
        }
    });
});

app.get('/api/data/:device/:session', function(req, res) {
    var device = req.params.device;
    var session = req.params.session;
    dbs.findDocs('data_' + device + '_' + session, null, function(err, docs) {
        if (isCorrect(err, docs, res)) {
            //console.log("Lookup done");
            tools.send_data(res, { result: "ok", data : docs });
        }
    });
});

app.get('/api/data/:device/:session/:field/:operator/:value', function(req, res) {
    var device = req.params.device;
    var session = req.params.session;
    var field = req.params.field;
    var value = parseInt(req.params.value);
    if (isNaN(value) || value == null) {
        console.log("NaN");
        value = req.params.value;
    }
    var operator = req.params.operator;

    dbs.findDocs('data_' + device + '_' + session, { [field] : { [operator] : value}}, function(err, docs) {
        if (isCorrect(err, docs, res)) {
            console.log("Lookup done with: " + field + " " + operator + " " + value);
            tools.send_data(res, { result: "ok", data : docs });
        }
    });
});

app.get('/api/data/:device/:session/:field/:value', function(req, res) {
    var device = req.params.device;
    var session = req.params.session;
    var field = req.params.field;
    var value = parseInt(req.params.value);
    if (isNaN(value) || value == null) {
        console.log("NaN");
        value = req.params.value;
    }

    dbs.findDocs('data_' + device + '_' + session, { [field] : value }, function(err, docs) {
        if (isCorrect(err, docs, res)) {
            console.log("Lookup done with: " + field + " " + value);
            tools.send_data(res, { result: "ok", data : docs });
        }
    });
});

app.get('/api/count/:device/:session/:field/:value', function(req, res) {
    var device = req.params.device;
    var session = req.params.session;
    var field = req.params.field;
    var value = req.params.value;

    dbs.countDocs('data_' + device + '_' + session, { [field] : value }, function(err, docs) {
        if (isCorrect(err, docs)) {
            console.log("Count done for: " + field);
            tools.send_data(res, { result: "ok", count : docs});
        }
    });
});

app.get('/api/count/:device/:session', function(req, res) {
    var device = req.params.device;
    var session = req.params.session;
    dbs.countDocs('data_' + device + '_' + session, null, function(err, docs) {
        if (isCorrect(err, docs, res)) {
            tools.send_data(res, { result: "ok", count : docs});
        }
    });
});

app.get('/api/types/:device/:session', function(req, res) {
    var device = req.params.device;
    var session = req.params.session;
    if (dbs.isNedb()) {
        dbs.findDocs('data_' + device + '_' + session, {}, function(err, result) {
            var grps = _.map(result, function(data) { return _.pluck(data.data, 'datatype'); });
            var red = _.reduce(grps, function(memo, item) { return memo.concat(item) }, []);
            var counts = _.countBy(red, function(item) { return item; });
            tools.send_data(res, {types : counts});
        });

    } else {
        var aggregator = [
            {
                "$unwind":"$data"
            },
            {
                "$group":{
                    "_id": {
                        "datatype":"$data.datatype"
                    },
                    "count":{
                        "$sum": 1
                    }
                }
            }
        ];
        dbs.aggregate('data_' + device + '_' + session, aggregator, function(err, docs) {
            if (isCorrect(err, docs, res)) {
                tools.send_data(res, {types : docs});
            }
        });
    }
});


app.get('/api/remove', function(req, res) {
    dbs.findDocs('devices', {}, function(err, devices) {
        console.log('found ' + util.inspect(devices));
        var removed = 0;
        async.each(devices, function(item, callback) {
            dbs.findDocs('sessions_' + item.device, {}, function(err, sessions) {
                console.log('found ' + util.inspect(sessions));
                async.each(devices, function(item, callback) {
                    dbs.removeDocs("data_" + item.device, {}, function(err, result) {
                        if (err)
                            callback(err)
                        else {
                            removed++;
                            callback();
                        }
                    });
                }, function(err) {
                    if (err)
                        callback(err);
                    else
                        callback();
                });
            });
        }, function(err) {
            if (isCorrect(err, "dummy", res)) {
                tools.send_data(res, { result: "ok", removed : removed});
            }
        });
    });
});

app.post('/api/device/:device/:session', function(req, res) {
    var device = req.params.device;
    var session = req.params.session;
    var data = req.body;

    if (req.raw != null) {
        console.log("Raw data = " + req.raw);
        data = JSON.parse(req.raw);
    }
    if (data == null) return;
    if (device == null) return;
    //console.log("Device " + device + ", data:" + util.inspect(data));
    dbs.updateDoc("devices", { device: device }, { device: device, time: new Date(), data: data.values }, function(err, result) {
        if (err)
            handleError(res, err);
        else {
            tools.send_data(res, { result: "ok", updated : result} );
        }
    });

});

app.post('/api/data/:device/:session', function(req, res) {
    var device = req.params.device;
    var session = req.params.session;
    var data = req.body;
    if (req.raw != null) {
        console.log("Raw data = " + req.raw);
        data = JSON.parse(req.raw);
    }
    if (data == null) return;
    if (data.array == null) {
        handleData(device, session, data, function(err) {
            if (err) {
                console.log('Error in async function: ' + err + ", " + util.inspect(results));
                handleError(res, err);
            } else {
                //console.log('Results of NeDB data operations: ' + util.inspect(results, {depth: null}));
                tools.send_data(res, { result: "ok", inserted : 1} );
                io.to("Plusot_" + device + "_" + session).emit('data', data);
            }
        });
    } else {
        async.each(data.array, function(data, functionCallback) {
            handleData(device, session, data, functionCallback);
        }, function(err) {
            if (err) {
                console.log('Error in async function: ' + err);
                handleError(res, err);
            } else {
                //console.log('Results of NeDB data operations: ' + util.inspect(results, {depth: null}));
                tools.send_data(res, { result: "ok", inserted : data.array.length} );  
            }
        });
    }
});

function handleData(device, session, data, functionCallback) {     
    async.series({
        addDevice: function(callback) {

            dbs.findDocs("devices", { device: device }, function(err, docs) {
                if (err)
                    callback(err, "Could not find device");
                else {
                    if (docs.length == 0) {
                        dbs.insertDoc("devices", { device: device, time: new Date() }, function(err, result) {
                            if (err)
                                callback(err, "Could not save device");
                            else
                                callback(null, result);
                        });
                    } else
                        callback(null, docs);
                }
            });
        },
        addSession: function(callback) {

            dbs.findDocs("sessions_" + device, { session: session }, function(err, docs) {
                if (err)
                    callback(err, "Could not find session");
                else {
                    if (docs.length == 0) {
                        dbs.insertDoc("sessions_" + device, {session: session, device: device, time: new Date() }, function(err, result) {
                            if (err)
                                callback(err, "Could not save session");
                            else
                                callback(null, result);
                        });
                    } else
                        callback(null, docs);
                }
            });
        },
        insertDoc: function(callback) {
            //console.log("Trying to handle: " + util.inspect(data, {depth: null}));
            data.device = device;
            dbs.insertDoc("data_" + device + "_" + session, data, function(err, result) {
                if (err)
                    callback(err, "Could not save data");
                else {
                    callback(null, {total: result.total});
                    //console.log("Inserted session: " + session);
                }
            });
        }
    }, function(err, results) {
        if (err) {
            console.log('Error in handleData: ' + err + ", " + util.inspect(results));
            //handleError(res, err);
            functionCallback(err);
        } else {
            //console.log('Results of NeDB data operations: ' + util.inspect(results, {depth: null}));
            //tools.send_data(res, { result: "ok", inserted : results.insertDoc.total} );
            //io.to("Plusot_" + device + "_" + session).emit('data', data);
            io.to("Plusot_" + device + "_" + session).emit('content', data);
            functionCallback(null);
        }
    });
}

function handleSession(device, session, data, functionCallback) {     
    async.series({
        addDevice: function(callback) {

            dbs.findDocs("devices", { device: device }, function(err, docs) {
                if (err)
                    callback(err, "Could not find device");
                else {
                    if (docs.length == 0) {
                        dbs.insertDoc("devices", { device: device, time: new Date() }, function(err, result) {
                            if (err)
                                callback(err, "Could not save device");
                            else
                                callback(null, result);
                        });
                    } else {
                        callback(null, docs);
                    }
                }
            });
        },
        addSession: function(callback) {
            dbs.findDocs("sessions_" + device, { session: session }, function(err, docs) {
                if (err)
                    callback(err, "Could not find session");
                else {
                    if (docs.length == 0) {
                        dbs.insertDoc("sessions_" + device, {session: session, device: device, time: new Date(), data: data }, function(err, result) {
                            if (err)
                                callback(err, "Could not save session");
                            else
                                callback(null, result);
                        });
                    } else {
                        dbs.updateDoc("sessions_" + device, { session: session }, {session: session, device: device, time: new Date(), data: data }, function(err, result) {
                            if (err)
                                callback(err, "Could not save session");
                            else {
                                //tools.send_data(res, { result: "ok", updated : result} );
                                callback(null, result);
                            }
                        });
                    }

                }
            });
        },
    }, function(err, results) {
        if (err) {
            console.log('Error in handleSession: ' + err + ", " + util.inspect(results));
            //handleError(res, err);
            functionCallback(err);
        } else {
            io.to("Plusot_" + device + "_" + session).emit('session', data);
            functionCallback(null);
        }
    });
}

io.on('connection', function (socket) {
    //console.log("New socket: " + util.inspect(socket));
    socket.emit('connect_message', { data: 'Meterz on ' + tools.fileDateLong() });
    socket.on('login', function (data) {
        console.log('SocketIO login for ' + data.name + " " + data.deviceid + " in room " + data.room);
        socket.emit('login', { result : "ok"});
        socket.join(data.room);
        socket.room = data.room;
        socket.to(data.room).emit('joined', data);
        //        console.log("Socket: " + util.inspect(socket));
        //        console.log("Socket remote address: " + socket.client.conn.remoteAddress);
        console.log("Handshake address: " + socket.handshake.address);

    });
    socket.on('content', function (dataObj) {
        //console.log('Socket.on: content = ' + util.inspect(dataObj, { showHidden: true, depth: null })); // JSON.stringify(data));
        if (dataObj.data == null || dataObj.deviceid == null) {
            socket.emit('content', { result : "error", info: "incorrect data" });
            return;
        }

        dataObj.data.device = dataObj.deviceid;
        var session = dataObj.sessionid;
        handleData(dataObj.deviceid, session, dataObj.data, function(err) {
            if(err)
                socket.emit('content', { result : "error", info: err, time: dataObj.data.time });
            else
                socket.emit('content', { result : "ok", time: dataObj.data.time });
        })

        //if (socket.room != null) socket.to(socket.room).emit('data', data.data);
    });
    socket.on('session', function (sessionObj) {
        //console.log('Socket.on: content = ' + util.inspect(dataObj, { showHidden: true, depth: null })); // JSON.stringify(data));
        if (sessionObj.data == null || sessionObj.deviceid == null) {
            socket.emit('session', { result : "error", info: "incorrect data" });
            return;
        }

        sessionObj.data.device = sessionObj.deviceid;
        var session = sessionObj.sessionid;
        handleSession(sessionObj.deviceid, session, sessionObj.data, function(err) {
            if(err)
                socket.emit('session', { result : "error", info: err, time: sessionObj.data.time });
            else
                socket.emit('session', { result : "ok", time: sessionObj.data.time });
        })

        //if (socket.room != null) socket.to(socket.room).emit('data', data.data);
    });
    socket.on('time', function (data) {
        console.log('SocketIO Time request: ' + JSON.stringify(data));
        socket.broadcast.emit('time_broadcast', data);

    });
});

app.get('*', function (req, res) {
    tools.send_data(res, { result: "error", info: "invalid url: " + req.path});
});

function getStaticSensorInfo() {
    var filePath = path.join('files/json', 'sensorinfo.json');

    fs.readFile(filePath, 'utf8', function (err, data) {
        if (err) {
            console.log("Could not read " + filePath + ": " + err);
            return;
        }
        var sensorInfo = JSON.parse(data);
        for (var address in sensorInfo) {
            if (staticSensors[address] == null) staticSensors[address] = {};
            staticSensors[address].info = sensorInfo[address];
            //console.log(address + " = " + JSON.stringify(sensorInfo[address]));
        }
    });
}

getStaticSensorInfo();

http.listen(gl.server_port);

var httpsServer = https.createServer(sslOptions, app);
httpsServer.listen(gl.ssl_server_port);

io.attach(httpsServer);

var fileDate = tools.fileDateLong();
async.series({
    init: function(callback) {
        dbs.connect(gl.use_mongo, callback);
    },
    //    insert: function(callback) {
    //        dbs.insertDoc('data', {name: 'wolter', gender: 'male', time: new Date()}, callback);
    //    },
    //    find: function(callback) {
    //        dbs.findDocs('data', {}, callback);
    //    },
    update: function(callback) {
        dbs.updateDoc('devices', {device: "324034" }, { device: "324034", time: new Date() }, callback);
    },
    count: function(callback) {
        dbs.countDocs('data', {}, callback);
    }
    //    group: function(callback) {
    //        dbs.findDocs('data', {}, function(err, result) {
    //            var grps = _.map(result, function(data) { return _.pluck(data.data, 'datatype'); });
    //            var red = _.reduce(grps, function(memo, item) { return memo.concat(item) }, []);
    //            var counts = _.countBy(red, function(item) { return item; });
    //            callback(null, counts);
    //        });
    //    },
    //    aggregate: function(callback) {
    //        dbs.aggregate('data', function(obj) { return obj.data[0].datatype}, callback);
    //    },
    //    remove: function(callback) {
    //        dbs.removeDocs('data', {}, callback);
    //    }
    //    close: function(callback) {
    //        dbs.close(callback);
    //    }
}, function(err, results) {
    if (err) {
        console.log('Error in async function: ' + err);
    } else {
        console.log('Results of NeDB operations: ' + util.inspect(results, {depth: null}));
    }
});
console.log('MeterzServe started ' + fileDate + ' running on port: ' + gl.server_port + ' and ' + gl.ssl_server_port);

//eof






